# Projet tinyzone par Eve-Anne, Maxime, Virginie et Justin

*  Verifier que la langue de toutes les pages sont en francais dans en-tete

 



1. Création projet tinyzone sur GitLab 
2. Création branche dev
3. Chacun s'occupe de l'html et du CSS de sa page (accueil: Eve-Anne, equipe: Maxime, produits et gammes: Virginie, contact: Justin)
4. Création dossier images contenant les images de tous 
5. Création des pages header.html et footer.html et inclusion du header et du footer en php sur chaque page
6. ajout script js sur page accueil et page equipe
7. Conversion des .html en .php 
8. Création dossiers partials et web
9. Ajout de nos pages .php, dossier css et dossier images dans web
10. Ajout de header.php et footer.php dans partials
11. Création d'un fichier commun database.sql et conversion en database.db ajoutés dans web 
12. Appel database.db sur chaque page en php
13. Ajout fichier évaluation par autre groupe (Evaluation_site), prise en compte et corrections




# Mise en place du footer
* Copier le code HTML du fichier footer.html dans votre page HTML
* Appeler dans le <head> de votre page la feuille de style footer.css qui se trouve dans le repertoire css
* Footer avec block en ligne et utilisation de flex

#Mise en place du include footer et header.php

* Remplacer de tout le code allant de <header>....</header> par <?php include "../partials/header.php";?>
* Idem pour le footer a remplacer par footer.php
  
# ATTENTION les chemins des liens ont également changé : 
* Pour les css le chemin d'acces est : ./css/style.css

# Mise à jour du site de la semaine du 04 février 2019 au 15 février 2019:
* Ajout de script js sur la page accueil et équipe. Création fichiers sql individuels spécifiques pour chaque page.
* Rassemblement des fichiers sql dans un seul et unique fichier database.sql.
* Convertion database.sql en database.db.

  
