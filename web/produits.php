
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width= <device-width>, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" type="text/css" href="./css/style.css">
    <link rel="stylesheet" type="text/css" href="./css/header.css">
    <link rel="stylesheet" type="text/css" href="./css/footer.css">
    <link rel="stylesheet" type="text/css" href="./css/produits.css">
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">

    <title>Nos Produits & Prix</title>
</head>

<body>
    <?php include "../partials/header.php";?>

    <div class="bandeau_pdts">
        <img src="./images/bandoproduit.jpg" alt="image illustrant produits" width="100%" height="500px"></div>
    <h1>Nos Produits et prix </h1>
    <div class="produit_flex">
     <div class="presentation-pdt">
        <?php
$db = new SQLite3('database.db');
$results = $db->query('SELECT * FROM produits'); ?>

        <?php 
while ($row = $results->fetchArray()) {
?>
        <div class="produits">
            <div class="img-produit"> <img src="<?php echo($row['img']); ?>" align="left">  </div>
           <h2>
                <?php echo($row['nompdt']); ?>
            </h2>

            <h3>Description :</h3>
            <p class="produits">
                <?php echo($row['descrip']);?>
            </p>

            <h3>Prix :
                <?php echo($row['price']);?><span> euro</span></h3>

            <p class="produits"> </p><br><br><br>
        </div>
        <?php } ?>
    </div>
    </div>

    <div style="clear:both;"></div>

    <?php include "../partials/footer.php";?>
</body>

</html>