<?php
            $db = new SQLite3('database.db');

            $results = $db->query('SELECT * FROM accueil');
            ?>
<!DOCTYPE html>
<html lang="fr">
    
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="./css/style.css"/>
    <link rel="stylesheet" type ="text/css" href="./css/accueil.css"/>
    <link rel="stylesheet" type ="text/css" herf="./css/footer.css"/>
    <link rel="stylesheet" type ="text/css" herf="./css/header.css"/>
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet"> 
    <title>Accueil</title>

    
    </head>
<body>

   

    <header>
        <?php include "../partials/header.php";?>
    </header>

    <main>
        
    
    
     
        <div class="accueilPartieLaTinyHouse">

            <h2 class="accueilTitreLaTinyHouse">La Tiny House</h2>
            <img class="camion" src="./images/camtar.png" alt="petit camion">
            <!--Script Camion Début -->
            <script>//on a créé une fonction qui active un deplacement d'une img gpg aux moment du clique de la souris sur la dite image
                    camion.onclick = function() {
                    let start = Date.now(); 
                    function draw(timePassed) {
                        // on demande au camion de se deplacer de 5 px
                    train.style.left = timePassed / 5 + 'px';

                                            };



                    let timer = setInterval(function() {
                    let timePassed = Date.now() - start;

                     camion.style.left = timePassed / 5 + 'px';
                     // on dit au camion de s'arreter apres 3200 secondes

                    if (timePassed > 3200) clearInterval(timer);

                    

                    }, 20);
                 };
                 
                </script>


            <p class="accueilContenuLaTinyHouse">Nomade, sur roues, disponible en kit ou livrée clés en main, la Tiny House vous permet de profiter de tout le nécessaire pour vivre... en voyageant. Pensée écologiquement, durablement et ne nécessitant pas de permis de construire, la Tiny' comme on la surnomme, est une véritable révolution dans le domaine de l'habitat insolite en Europe.</p>
        </div>

            

        <?php while ($row = $results->fetchArray()){ ?>

            <div class="accueilPartieOptimisation">

                <img class="accueilImageOptimisation" src="<?php echo $row['image']; ?>" alt="<?php echo $row['alt']; ?>">

    

            <div class="accueilOptimisationTitreContenu">
                    <h2><?php echo $row['titre']; ?> </h2>  
                    <p><?php echo $row['contenu']; ?> </p>
            </div>
        </div> 
                <?php } ?>

        
            

    </main>
    <div style="clear:both;"></div>

            <?php include "../partials/footer.php";?>
     
    
</body>

</html>
