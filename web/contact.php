<!DOCTYPE html>
<?php 
 $db = new SQLite3('database.db');
 ?>
<?php $results = $db->query('SELECT * FROM "contact" WHERE id = 1');
 $contact = $results -> fetchArray();?>
<html lang="fr">

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="./css/style.css">
  <link rel="stylesheet" type="text/css" href="./css/contact.css">
  <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">
  <title>Contact</title>
</head>

<body>
  <header>
    <?php include "../partials/header.php" ;?>
  </header>
  <div class="pageAlignement">
    <h1>Contact en france</h1>
    <?php echo ($contact ['Prenom']);?>
    <p class=Contact-Block>Contactez-nous directement depuis ce formulaire. Exposez-nous vos projets, demandez-nous un
      devis ou plus d’informations… vos messages nous sont envoyés instantanément. N’oubliez pas de nous préciser le
      type de produit qui vous intéresse et tous les détails qui pourraient avoir une importance, pour vous donner une
      réponse précise. De notre côté, nous vous répondrons le plus rapidement et efficacement possible ! Merci.</p>
    <img contact-arierimg src="./images/Coordoner.png">
    <iframe contact-arierimg src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2792.0160580313745!2d5.918449750990804!3d45.59022193294419!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478ba87d244df4f5%3A0x8a23de29d967f2ae!2sLa+Dynamo!5e0!3m2!1sfr!2sfr!4v1549284488633"
      width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
  <!--
  <div>
    <p>
      <form method="post" action="mail.php">
        <fieldset>
          <legend> Nous Contactez: </legend>
          <label for="Pseudo">Pseudo:(obligatoire)</label><input required maxlength="20" placeholder="exemple:lostxxplayerz"
            id="Pseudo" type="text" name="Pseudo" />
          <label for="Nom">Nom:(obligatoire)</label><input required placeholder="exemple:Smith" id="Nom" type="text"
            name="Nom" />
          <label for="Prenom">Prenom:(obligatoire)</label><input required placeholder="exemple:Will" id="Prenom" type="text"
            name="Prenom" />
          <label for="Email">Email:(obligatoire)</label><input required placeholder="exemple:exemple@exemple.fr" id="Email"
            type="email" name="Email" />
          <label for="Message"> un message a nous faire passer ?</label>
          <textarea class="Boitedetext" name="Message" id="Message" type="text">Une question ? 
          N'ésitez pas a nous contacter via le formulaire pour toute demande</textarea>
          <input type="submit" value="Envoyer" />
        </fieldset>
      </form>
    </p>
  </div> 
  -->
  <div style="clear:both;"></div>
  <?php include "../partials/footer.php" ;?>

</html>