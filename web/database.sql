CREATE TABLE accueil (
	id INTEGER,
	titre TEXT,
	contenu TEXT,
	image TEXT,
        alt TEXT
);
INSERT INTO accueil
(id, titre, contenu, image, alt)
VALUES(1, 'Optimisation de l''espace', 'Ces maisons minuscules sont en principe montées sur roues. Assimilées aux caravanes, elles ne nécessitent pas de permis de construire. Vous pouvez alors enfin exploiter votre terrain agricole ou non constructible et avoir la sensation d''être dans une vraie petite maison. Le mouvement autour du petit habitat s’est véritablement mis en route car il répond aux préoccupations urbanistiques actuelles; réduction de l’espace, durabilité de l’habitat, et coût abordable. Un choix de vie vers plus de simplicité afin de se donner du temps pour soi et les autres', './images/accphoto1.jpg', 'mezzanine');
INSERT INTO accueil
(id, titre, contenu, image, alt)
VALUES(2, 'Vivre petit, Living simply', 'Ma Petite Maison est soucieuse de la qualité des équipements qui composeront votre Tiny House. Nous avons pour priorité d’utiliser des matériaux haut de gamme et qui respectent l’environnement. Ne nécessitant pas de permis de construire, elle dispose cependant de tout le nécessaire pour y vivre confortablement au quotidien et respecte les législations en vigueur : une pièce à vivre spacieuse, pouvant contenir un petit salon, un espace cuisine, une salle de bain et des sanitaires, ainsi qu’une mezzanine conçue pour supporter un lit 2 places. Vous souhaitez une deuxième mezzanine pour vos enfant ou accueillir des amis ? Aucun problème !', './images/tiny8.jpg', 'exemple');
INSERT INTO accueil
(id, titre, contenu, image, alt)
VALUES(3, 'Un refuge idéal', 'Posée au fond d’un jardin, pour un studio d’adolescent ou une chambre d’amis, la Tiny House répond à tous les besoins et à toutes les envies. Perchée en haut d’une montagne, elle sera le refuge idéal pour des vacances au sports d’hiver ou cachée dans une forêt, comme une tanière pour un repos bien mérité. Parce-qu’avec la Tiny House, tous les chemins ne vous mèneront pas seulement à Rome… ', './images/tiny9.jpg', 'intérieur');
CREATE TABLE produits (
	id INTEGER,
	nompdt TEXT,
	descrip TEXT,
	img TEXT,
	price TEXT
);
INSERT INTO produits
(id, nompdt, descrip, img, price)
VALUES(1, 'Esmeralada', 'Esmeralda bois rouge', 'images/esmeralda_petit.jpg', '20000');
INSERT INTO produits
(id, nompdt, descrip, img, price)
VALUES(2, 'Ambre', 'Lodge ambre 30M² cabane avec terrasse', 'images/LodgeAmbre_petit.jpg', '50000');
INSERT INTO produits
(id, nompdt, descrip, img, price)
VALUES(3, 'Dakota', 'Lodge Dakota 30M² cabane avec terrasse', './images/LodgeDakota_petit.jpg', '50000');
INSERT INTO produits
(id, nompdt, descrip, img, price)
VALUES(4, 'Dôme', 'Le dôme cette structure parfaite issue de la nature offre un espace unique et fonctionnel, adapté à tous les usages.', './images/Photos-dômes-vrac-36-150x150.jpg', '80000');
CREATE TABLE gamme (
	id INTEGER,
        name_gamme TEXT,
	image TEXT
);
INSERT INTO gamme
(id, name_gamme, image)
VALUES(1, 'Nano Habitat', 'eco.jpg
');
INSERT INTO gamme
(id, name_gamme, image)
VALUES(2, 'Tiny House', 'basic.jpg
');
INSERT INTO gamme
(id, name_gamme, image)
VALUES(3, 'Dome', 'middle.jpg');
INSERT INTO gamme
(id, name_gamme, image)
VALUES(4, 'Crystal-Dome', 'crystal.jpg');
CREATE TABLE equipe (
	id INTEGER,
	name TEXT,
	"function" TEXT,
	image TEXT,
        alt TEXT,
        js TEXT
);
INSERT INTO equipe
(id, name, "function", image, alt, js)
VALUES(1, 'Eve-Anne', 'Directrice Générale', './images/girl.png', 'avatar1', 'Coucou!');
INSERT INTO equipe
(id, name, "function", image, alt, js)
VALUES(2, 'Maxime', 'Directeur Technique', './images/boy.png', 'avatar2', 'Hello!');
INSERT INTO equipe
(id, name, "function", image, alt, js)
VALUES(3, 'Virginie', 'Chef de projet', './images/girl(1).png', 'avatar3', 'Salut!');
INSERT INTO equipe
(id, name, "function", image, alt, js)
VALUES(4, 'Justin', 'Directeur commercial', './images/man.png', 'avatar4', 'Hey!');
CREATE TABLE contact (
	id INTEGER,
	Prenom TEXT,
	 Nom TEXT,
         Email TEXT,
	jour TEXT
);
INSERT INTO Contact
(id, Prenom, Nom, Email, jour)
VALUES(1, 'Nous Contacter', 'dole', 'justin.dole.simplon@gmail.com', 'mardi');
INSERT INTO Contact
(id, Prenom, Nom, Email, jour)
VALUES(2, 'maxime', 'chofflet', 'maxime.chofflet.simplon@gmail.com', 'mardi');
INSERT INTO Contact
(id, Prenom, Nom, Email, jour)
VALUES(3, 'eve-anne', 'rousselin', 'eveanne.rousselin.simplon@gmail.com', 'mardi');
INSERT INTO Contact
(id, Prenom, Nom, Email, jour)
VALUES(4, 'virginie', 'plantade', 'virginieplantade@yahoo.fr', 'mardi');



