<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=<device-width>, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" type="text/css" href="./css/style.css">
    <link rel="stylesheet" type="text/css" href="./css/header.css">
    <link rel="stylesheet" type="text/css" href="./css/footer.css">
    <link rel="stylesheet" type="text/css" href="./css/produits.css">
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">

    <title>Nos Gammes</title>
</head>

<body>
    <?php include "../partials/header.php";?>

    <div class="bandeau_pdts">
        <img src="./images/bandoproduit.jpg" alt="image illustrant produits" width="100%" height="500px"></div>
    <div class="produitsprinc1">

        <h1>Nos gammes </h1>

        <img src="./images/eco.png" alt="Maison de la gamme eco" align="left">
        <h2>Nano Habitat</h2>

        <h3>Jusqu’à 20 m² sans permis de construire</h3>
        Si votre studio ne dépasse pas 20 m², vous n’aurez pas besoin de permis de construire.
        Tout dépend ensuite de la taille de votre NanoHabitat. En règle générale, une simple déclaration
        préalable en mairie sera suffisante. Il est possible d’aller au delà de 20 m²,
        un permis de construire devra être obtenu auprès de votre mairie. </p>

        <h3>Créez votre petit espace de liberté ! </h3>

        <p>Nous vous propose d’installer ou de vous aider à installer, en partenariat avec une équipe d’architectes et
            de techniciens, des structures modulaires et écologiques pour y créer votre propre espace de liberté,
            de loisir, de confort, pour un nouveau mode de vie ou de travail. Deux versions vous seront proposées.
            La version clé en main toute équipée et installée par notre équipe et la version KIT pré-câblée et isolée à
            installer par vos soins.

            Le Nano-habitat est le pionnier des studios et bureaux de jardins contemporains, à la fois design,
            qualitatif et éco-responsable. Bien loin des chalets et abris traditionnels, il s’agit d’une véritable
            solution pour agrandir facilement son habitation, et profiter de la nouvelle loi majorant les surfaces
            constructibles actuelles. </p><br><br><br>
    </div>

    <div class="gammes" id="basic">
        <h2>Tiny House</h2>
        <img src="./images/basic.jpg" alt="maison de la gamme basique" align="right">
        <h3>Toutes proportions gardées</h3>

        <p>Fusion entre la cabane et la roulotte, cette micro maison respecte tous les principes de base de
            l’architecture. Sa configuration classique ou contemporaine peut se constituer d’une toiture plate ou d’une
            toiture double pente couvrant une large pièce avec une chambre en mezzanine. Vous retrouverez également une
            cuisine et une salle d’eau, tout y est !
            A NOTER : Tous nos modèles sont modulables et adaptables selon vos envies ou besoins. Nous réalisons des
            tiny houses sur mesure en fonction de vos plans. Nous tenons à construire la tiny à votre image.</p>
        <h3>Optimisation de l’espace</h3>

        <p>Ces maisons minuscules sont en principe montées sur roues. Assimilées aux caravanes, elles ne nécessitent
            pas de permis de construire. Vous pouvez alors enfin exploiter votre terrain agricole ou non constructible
            et avoir la sensation d’être dans une vraie petite maison.
            Le mouvement autour du petit habitat s’est véritablement mis en route car il répond aux préoccupations
            urbanistiques actuelles ; réduction de l’espace, durabilité de l’habitat, et coût abordable. Un choix de
            vie vers plus de simplicité afin de se donner du temps pour soi et les autres.</p>
        <br><br><br><br><br><br><br><br>
    </div>

    <div class="gammes" id="middle">

        <img src="./images/dome.jpg" alt="maison moyenne gamme" align="left">
        <h2>Dômes</h2>
        <h3>Des espaces Zen et futuristes.</h3>

        <p> Le dôme, cette structure parfaite issue de la nature, offre un espace unique et fonctionnel, adapté
            à tous les usages. Nous combinons la géométrie sacrée de R. Buckminster Fuller et des
            couvertures spécialement conçues pour vous transporter dans un univers Zen et Futuriste. Nos dômes
            s’illuminent des lumières naturelles créant une atmosphère unique proche de la nature et confortable
            à fois. </p>

        <h3>Stables & résistants.</h3>
        <p>

            Adaptés au temps hivernal tout comme aux chaleurs de l’été, de plus en plus de personnes choisissent de
            vivre confortablement installées dans nos dômes, ou bien même d’y créer leurs événements (salon, mariage,
            festival…) ou leurs espaces de culture. Robustes, une structure en acier leur permet de résister aux
            lourdes charges de neige et aux ouragans. Disponibles en plusieurs tailles et résistantes aux intempéries,
            nos couvertures durables incluent fenêtres, portes et sorties pour y installer un de nos poêle à bois.</p>
        <br><br><br><br>
    </div>

    <div class="gammes" id="high">
        <h2>Crystal Dômes</h2>
        <img src="./images/crystal.jpg" alt="dôme de la gamme crystal" width="371px" height="500px" align="right">

        <h3>Votre rêve devient réalité ? </h3>
        <p>Avec son matelas sphérique d’un diamètre de 3 mètres et une hauteur de 4 mètres, « le Crystal Dome » peut
            très confortablement accueillir un couple d’amoureux ou même une petite famille pour une nuit. Quel bonheur
            de s’endormir en écoutant les sons de la nature et en observant les étoiles filantes</p>


        <h3>Techniques de construction innovantes</h3>
        <p><b>Dimensions :</b> 4 mètres de haut / 3 mètres de diamètre.

            <p> <b>Poids :</b> 300 kg, résistant aux vents de plus de 250 km/h
            </p>
            <p>Le « Crystal Dome » peut-être posé sur des pieds au sol ou à plus de 4 mètres de haut relié par un
                escalier, ou encore fixé dans les arbres avec une passerelle d’accès.
            </p>
            <p>Il est parfaitement protégé des intempéries avec sa membrane en PVC imperméable d’une épaisseur de 630
                gr/m2.
            </p>
            <p>L’isolation est optimum par temps chaud ou froid grâce à sa structure thermique et phonique
                multi-couches.
            </p>
            <p>La conception du « Crystal Dome » permet une très bonne ventilation, avec une aération prévue prévue par
                le bas et une ventilation intégrée dans le haut de la structure.
            </p>
            <p>La structure est en acier galvanisé. Elle supporte un poids de plus de 1500 kg.</p>
        </p>
    </div>

    </div>


    <div style="clear:both;"></div>

    <?php include "../partials/footer.php";?>
</body>

</html>