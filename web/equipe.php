<?php $db = new SQLite3('database.db');?>
<?php $result = $db->query('SELECT * FROM equipe');?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">


  <link rel="stylesheet" type="text/css" href="./css/style.css">
  <link rel="stylesheet" type="text/css" href="./css/header.css">
  <link rel="stylesheet" type="text/css" href="./css/footer.css">
  <link rel="stylesheet" type="text/css" href="./css/equipe.css">


  <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">
  <title>Equipe</title>
</head>

<body>
  <?php include "../partials/header.php";?>

  <h1 class="team">Notre équipe</h1> <br>

  <?php while ($row = $result->fetchArray()) { ?>

  <div class="equipe">
    <div class="membersEquipe">
      <button onclick="alert('<?php echo $row['js'];?>')"><img src="<?php echo $row['image'];?>" alt="<?php echo $row['alt'];?>"></button>

      <h3>
        <?php echo $row['name'];?>
      </h3>
      <h3>
        <?php echo $row['function'];?>
      </h3>
    </div>
  </div>
  <?php } ?>

  <div style="clear:both;"></div>

  <?php include "../partials/footer.php";?>

</body>

</html>